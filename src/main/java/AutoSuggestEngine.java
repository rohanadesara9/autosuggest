import AutoSuggestService.CommandProcessor;
import AutoSuggestService.ProductAutoSuggest;
import AutoSuggestService.SearchAutoSuggest;
import entities.Command;
import CommandUtils.CommandMaker;
import dao.ProductRecordDaoImpl;
import dao.SearchRecordDaoImpl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;

public class AutoSuggestEngine {
    public static void main(String[] args) throws Exception{

        //initialize both tries
        ProductRecordDaoImpl productRecordDao = new ProductRecordDaoImpl();
        SearchRecordDaoImpl searchRecordDao = new SearchRecordDaoImpl();

//        Map<String, Integer> productMap = new HashMap<>();
        ProductAutoSuggest productAutoSuggest = new ProductAutoSuggest(productRecordDao);
        productAutoSuggest.load();
        SearchAutoSuggest searchAutoSuggest = new SearchAutoSuggest(searchRecordDao);
        searchAutoSuggest.load();

        runInputLoop(productAutoSuggest, searchAutoSuggest);
    }

    private static void runInputLoop(ProductAutoSuggest productAutoSuggest, SearchAutoSuggest searchAutoSuggest) throws SQLException, ExecutionException, InterruptedException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter command:");
        CommandProcessor commandProcessor = new CommandProcessor(productAutoSuggest, searchAutoSuggest);
        String input;
        while(!(input = sc.nextLine()).equalsIgnoreCase("exit")) {
            CommandMaker commandMaker = new CommandMaker();
            Command command = commandMaker.parseCommand(input.trim());
            commandProcessor.processCommand(command);
        }

        {
            System.out.println("Exiting..");
            System.exit(1);
        }
    }
}
