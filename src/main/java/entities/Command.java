package entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;

@AllArgsConstructor
@Getter
public class Command {
    @NonNull
    CommandType commandType;
    QueryType queryType;
    @NonNull
    String query;
    Integer score;
}
