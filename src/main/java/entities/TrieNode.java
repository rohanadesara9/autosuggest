package entities;

import java.util.HashMap;
import java.util.Map;

public class TrieNode {
    private char letter;
    private Map<Character, TrieNode> children;
    private boolean endOfWordFlag;



    private Integer popularityScore;

    /**
     * Initializes a TrieNode with its letter
     * @param letter The letter in this node
     */
    public TrieNode(char letter) {
        this.letter = letter;
        this.children = new HashMap<>();
    }

    public char getLetter() {
        return letter;
    }

    @Override
    public String toString() {
        return Character.toString(letter);
    }

    public Map<Character, TrieNode> getChildren() {
        return children;
    }

    public void setChildren(Map<Character, TrieNode> children) {
        this.children = children;
    }

    public boolean getEndOfWordFlag() {
        return endOfWordFlag;
    }

    public void setEndOfWordFlag(boolean endOfWordFlag) {
        this.endOfWordFlag = endOfWordFlag;
    }

    public Integer getPopularityScore() {
        return popularityScore;
    }

    public void setPopularityScore(Integer popularityScore) {
        this.popularityScore = popularityScore;
    }
}
