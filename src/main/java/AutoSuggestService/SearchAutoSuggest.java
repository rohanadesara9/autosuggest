package AutoSuggestService;

import dao.SearchRecordDaoImpl;
import entities.Record;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
public class SearchAutoSuggest extends AutoSuggest {
    private Map<String, Integer> searchMap;
    private SearchRecordDaoImpl searchRecordDao;

    public SearchAutoSuggest(SearchRecordDaoImpl searchRecordDao) {
        this.searchRecordDao = searchRecordDao;
        this.searchMap = new HashMap<>();
    }

    public void writeToDB(String text, Integer score) throws SQLException {
        this.searchRecordDao.addRecord(text, score);
        this.searchMap.put(text, score);
    }
    public SearchAutoSuggest load() throws SQLException {
        List<Record> records =  searchRecordDao.getRecords();
        records.forEach(r -> {
                this.addWord(r.getText(), r.getScore());
                searchMap.put(r.getText(), r.getScore());
        });
        return this;
    }
}
