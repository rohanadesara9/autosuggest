package AutoSuggestService;

import entities.Command;
import entities.CommandType;
import entities.QueryType;
import lombok.AllArgsConstructor;

import java.sql.SQLException;

import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

@AllArgsConstructor
public class CommandProcessor {
    private ProductAutoSuggest productAutoSuggest;
    private SearchAutoSuggest searchAutoSuggest;


    public void processCommand(Command command) throws SQLException, ExecutionException, InterruptedException {

        if(CommandType.INDEX.equals(command.getCommandType())) {

            if(QueryType.PRODUCT.equals(command.getQueryType())) {
                //write to db
                productAutoSuggest.writeToDB(command.getQuery(), command.getScore());
                //make call to add query to trie
                productAutoSuggest.addWord(command.getQuery(), command.getScore());
            } else {
                searchAutoSuggest.writeToDB(command.getQuery(), command.getScore());
                searchAutoSuggest.addWord(command.getQuery(), command.getScore());
            }

        } else {
            //command is search
            ExecutorService executor = Executors.newFixedThreadPool(2);
            Future<List<String>> futureProduct = executor.submit(new Callable<List<String>>() {
                public List<String> call() throws Exception {
                    return productAutoSuggest.autoComplete(command.getQuery());
                }
            });
            Future<List<String>> futureSeaarch = executor.submit(new Callable<List<String>>() {
                public List<String> call() throws Exception {
                    return searchAutoSuggest.autoComplete(command.getQuery());
                }
            });

            //get top 5 suggestions
            List<String> productSuggestions = getListByPopularityScore(futureProduct.get(), productAutoSuggest.getProductMap());
            List<String> searchSuggestions = getListByPopularityScore(futureSeaarch.get(), searchAutoSuggest.getSearchMap());

            System.out.print("Product Terms List: ");
            System.out.println(productSuggestions);
            System.out.print("Search Terms List: ");
            System.out.println(searchSuggestions);
        }


    }

    private List<String> getListByPopularityScore(List<String > resultList,  Map<String, Integer> anyMap) {
        Map<String, Integer> intersectionMap = new HashMap<>();
        resultList.forEach(l -> {
            if(anyMap.containsKey(l))
                intersectionMap.put(l, anyMap.get(l));
        });
        return sortByValue(intersectionMap);
    }

    private List<String> sortByValue(Map<String, Integer> intersectionMap)
    {
        // Create a list from elements of HashMap
        List<Map.Entry<String, Integer> > list =
                new LinkedList<Map.Entry<String, Integer> >(intersectionMap.entrySet());

        // Sort the list
        Collections.sort(list, new Comparator<Map.Entry<String, Integer> >() {
            public int compare(Map.Entry<String, Integer> o1,
                               Map.Entry<String, Integer> o2) {
                return (o2.getValue()).compareTo(o1.getValue());
            }
        });

        return list.stream()
                .map(l -> l.getKey())
                .collect(Collectors.toList())
                .subList(0,Math.min(list.size(), 5));
    }
}
