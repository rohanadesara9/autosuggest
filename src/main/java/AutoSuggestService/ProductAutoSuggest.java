package AutoSuggestService;

import dao.ProductRecordDaoImpl;
import entities.Record;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
public class ProductAutoSuggest extends AutoSuggest {

    Map<String, Integer> productMap;
    ProductRecordDaoImpl productRecordDao;
    public ProductAutoSuggest (ProductRecordDaoImpl productRecordDao) {
        this.productRecordDao = productRecordDao;
        this.productMap = new HashMap<>();
    }
    public void writeToDB(String text, Integer score) throws SQLException {
        this.productRecordDao.addRecord(text, score);
        productMap.put(text, score);
    }


    public ProductAutoSuggest load () throws SQLException {
        List<Record> records =  productRecordDao.getRecords();
        records.forEach(r -> {
            this.addWord(r.getText(), r.getScore());
            productMap.put(r.getText(), r.getScore());
        });
        return this;
    }
}
