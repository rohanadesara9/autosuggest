package dao;

import entities.Record;

import java.sql.SQLException;
import java.util.List;

public interface RecordDao {
    public List<Record> getRecords() throws SQLException;
    public void addRecord(String text, Integer score) throws SQLException;
}
