package dao;

import entities.Record;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProductRecordDaoImpl implements RecordDao{
    Statement statement;
    public ProductRecordDaoImpl() throws SQLException, ClassNotFoundException, IllegalAccessException, InstantiationException {
        Class.forName(("com.mysql.jdbc.Driver")).newInstance();
        Connection con= DriverManager.getConnection(
                "jdbc:mysql://127.0.0.1:3306/Sherlock","root","root");
        statement = con.createStatement();
    }

    @Override
    public List<Record> getRecords() throws SQLException {
        String sqlQuery = "select * from productrecords";
        List<Record> ls = new ArrayList<>();
        ResultSet rs = executeGetQuery(sqlQuery);
        while(rs!=null && rs.next()) {
            ls.add(new Record(rs.getString("text"), rs.getInt("score")));
        }
        return ls;
    }

    @Override
    public void addRecord(String text, Integer score) throws SQLException {
        String sqlQuery = "insert into productrecords (text, score) values ";
        executeSetQuery(sqlQuery, text, score);
    }

    private void executeSetQuery(String sqlQuery, String text, Integer score) throws SQLException {
        this.statement.execute(sqlQuery + "(\"" + text + "\", " + score + ") ON DUPLICATE KEY UPDATE score =" + score + ";");
    }

    ResultSet executeGetQuery(String sqlQuery) throws SQLException {
        return this.statement.executeQuery(sqlQuery);
    }
}
