package CommandUtils;

import entities.Command;
import entities.CommandType;
import entities.QueryType;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@NoArgsConstructor
public class CommandMaker {

    final String indexPattern = "INDEX[\\s]+[SEARCH|PRODUCT]+[\\s]+\"[a-zA-Z0-9\\s]+\"[\\s]+[0-9]+";
    final String searchPattern = "SEARCH[\\s]+\"[a-zA-Z0-9\\s]+\"";
    final String queryPattern = "\"[a-zA-Z0-9\\s]+\"";

    public Command parseCommand(String commandStr) {
        CommandType commandType;
        Integer score = null;
        if(Pattern.matches(indexPattern, commandStr)){
            commandType = CommandType.INDEX;
        } else if(Pattern.matches(searchPattern, commandStr)) {
            commandType = CommandType.SEARCH;
        } else {
            throw new IllegalArgumentException("Enter command properly!");
        }

        List<String> list = Arrays.stream(commandStr.split(" ")).collect(Collectors.toList());

        //query type && score
        QueryType queryType = null;
        if(commandType == CommandType.INDEX) {
            if(list.get(1).equalsIgnoreCase("SEARCH"))
                queryType = QueryType.SEARCH;
            else
                queryType = QueryType.PRODUCT;

            //score
            Pattern scorePattern = Pattern.compile("[0-9]+$");
            Matcher matcher1 = scorePattern.matcher(commandStr);
            matcher1.find();
            score = Integer.parseInt(matcher1.group());

        }

        //query
        Pattern queryPattern1 = Pattern.compile(queryPattern);
        Matcher matcher = queryPattern1.matcher(commandStr);
        matcher.find();
        String query = matcher.group();
        query = query.replace("\"", "");

        return new Command(commandType, queryType, query, score);
    }
}