Command syntax:
INDEX SEARCH|PRODUCT <search_term: string> <score: int>
OR
SEARCH <search_term: string>
Note: search_term should be in quotes.

SQL DB and Tables schema:
CREATE DATABASE `sherlock` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
CREATE TABLE `productrecords` (
  `text` varchar(45) NOT NULL,
  `score` int NOT NULL,
  PRIMARY KEY (`text`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
CREATE TABLE `searchrecords` (
  `text` varchar(45) NOT NULL,
  `score` int NOT NULL,
  PRIMARY KEY (`text`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
